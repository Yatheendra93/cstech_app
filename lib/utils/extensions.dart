extension EmailValidator on String {
  bool isValidEmail() {
    if (isEmpty) {
      return false;
    } else {
      return true;
    }
  }
// bool isValidEmail() {
//   return RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$').hasMatch(this);
// }
}

extension PasswordValidator on String {
  bool isValidPassword() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension ProfileValidator on String {
  bool isProfile() {
    if (isEmpty) {
      return false;
    } else {
      return true;
    }
  }
}

extension JobTypeValidator on String {
  bool isJobType() {
    if (isEmpty) {
      return false;
    } else {
      return true;
    }
  }
}

extension HotelValidator on String {
  bool isValidHotel() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension SurNameValidator on String {
  bool isSurName() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension FirstNameValidator on String {
  bool isFirstName() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension TelNumberValidator on String {
  bool isTelNumber() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension HouseNoValidator on String {
  bool isHouseNo() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension PostCodeValidator on String {
  bool isPostCode() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension NationalityValidator on String {
  bool isNationality() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension AddmissionValidator on String {
  bool isAddmission() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension IBANValidator on String {
  bool isIBAN() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension BICValidator on String {
  bool isBIC() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}

extension SignatureValidator on String {
  bool isSignature() {
    if (isEmpty || length < 6) {
      return false;
    } else {
      return true;
    }
  }
}
