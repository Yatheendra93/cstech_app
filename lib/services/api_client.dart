import 'dart:convert';
import 'package:devaseva_app/screens/educationList_screen.dart';
import 'package:flutter/material.dart';
import 'package:devaseva_app/models/educationById_Model.dart';
import 'package:devaseva_app/models/education_Model.dart';
import 'package:devaseva_app/models/loginResponse_Model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;


class ApiClient{

  Future<LoginResponse> loginApi({required BuildContext context, required String username, required String password}) async {
    try {
      var url = Uri.parse('https://testitecywebapi.azurewebsites.net/Jobseeker/Login/$username/$password');
      final response = await http.get(url);
      LoginResponse exData = LoginResponse.fromJson(jsonDecode(response.body));

      print("Response :: ${response.body}");

     return exData;
    } catch (e) {


      print("Error ::: $e");

      rethrow;
    }

  }


  Future<EducationResponse> fetchData() async {
    final response = await http.get(Uri.parse(
        'http://testitecywebapi.azurewebsites.net/Jobseeker/getEducation'));
    if (response.statusCode == 200) {
      final result = EducationResponse.fromJson(json.decode(response.body));
      print("Res::: $response");
      return result;
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<EducationByIdResponse> fetchEducationDetails(int id) async {
    final response = await http.get(Uri.parse(
        'http://testitecywebapi.azurewebsites.net/Jobseeker/getSpecialization/$id'));
    if (response.statusCode == 200) {
      final result = EducationByIdResponse.fromJson(json.decode(response.body));
      print("Res::: $response");
      return result;
    } else {
      throw Exception('Failed to load data');
    }
  }

}