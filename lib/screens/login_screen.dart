// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unused_local_variable, unnecessary_new

import 'dart:convert';
import 'package:devaseva_app/Services/api_client.dart';
import 'package:devaseva_app/models/loginResponse_Model.dart';
import 'package:devaseva_app/screens/educationList_screen.dart';
import 'package:devaseva_app/utils/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController pswdController = TextEditingController();

  bool showEmailError = false;
  bool showPswError = false;
  var isLoading = false;
  bool _password = true;
  AutovalidateMode  autovalid = AutovalidateMode.disabled;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Form(
          key: _formKey,
          autovalidateMode:  autovalid,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 100,
                width: 260,
                margin: EdgeInsets.symmetric(horizontal: 16),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  image: DecorationImage(
                    image: AssetImage('assets/images/appicon.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),

              // RichText(
              //   text: TextSpan(
              //     text: 'FOR',
              //     style: TextStyle(
              //         color: Colors.blue[900],
              //         fontSize: 40,
              //         fontWeight: FontWeight.w600,
              //         fontFamily: "Poppins"),
              //     children: [
              //       TextSpan(
              //         text: ' Testing',
              //         style: TextStyle(
              //             color: Colors.lightGreen,
              //             fontSize: 40,
              //             fontWeight: FontWeight.w600,
              //             fontFamily: "Poppins"),
              //       ),
              //     ],
              //   ),
              // ),

              SizedBox(
                height: 60,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                child: TextFormField(
                  textInputAction: TextInputAction.go,
                  controller: emailController,
                  validator: emailValidator,
                  style: TextStyle(color: Colors.grey),
                  decoration: InputDecoration(
                    prefixIcon: const Icon(
                      Icons.account_circle,
                      size: 24,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: BorderSide(
                          color: Colors.grey,
                        )),
                    labelText: "Email/Username",
                    isDense: true,
                    // errorStyle: TextStyle(fontSize: 1),
                    labelStyle: TextStyle(fontSize: 14, color: Colors.grey),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                child: TextFormField(
                  textInputAction: TextInputAction.go,
                  controller: pswdController,
                  obscureText: _password,
                  validator: pswValidator,
                  style: TextStyle(color: Colors.grey),
                  decoration: InputDecoration(
                    prefixIcon: const Icon(
                      Icons.lock,
                      size: 24,
                    ),
                    // errorStyle: TextStyle(fontSize: 1),
                    labelText: "Password",
                    labelStyle: TextStyle(fontSize: 14, color: Colors.grey),
                    isDense: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: BorderSide(
                          color: Colors.grey,
                        )),
                    suffixIcon: GestureDetector(
                      child: _password
                          ? Icon(
                              CupertinoIcons.eye,
                              color: Colors.black,
                              size: 20,
                            )
                          : Icon(
                              CupertinoIcons.eye_slash,
                              color: Colors.black,
                              size: 20,
                            ),
                      onTap: () {
                        setState(() {
                          _password = !_password;
                          // _isObscure = !_isObscure;
                        });
                      },
//                              onTap: _toggle,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              InkWell(
                onTap: isLoading
                    ? null
                    : () {
                        _validate();
                      },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Container(
                    height: 48,
                    child: Center(
                        child: isLoading
                            ? Loader()
                            : Text("Login",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "Poppins"))),
                    decoration: BoxDecoration(
                      color: Colors.blueAccent,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String? pswValidator(String? value) {
    RegExp regex =
        RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
    if (value!.isEmpty) {
      return 'Please enter password';
    } else {
      if (!regex.hasMatch(value)) {
        return 'Enter valid password';
      } else {
        return null;
      }
    }
  }

  String? emailValidator(String? value) {
    RegExp regex = RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    if (value!.isEmpty) {
      return 'Please enter email or username';
    } else {
      if (!regex.hasMatch(value)) {
        return 'Enter valid email id';
      } else {
        return null;
      }
    }
  }

  Future<void> _validate() async {
    setState(() {
      autovalid = AutovalidateMode.always;
    });
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      if (mounted) {
        setState(() {
          isLoading = true;
        });
      }

      final res = await ApiClient().loginApi(context: context, username: emailController.text.trim().toString(), password: pswdController.text.toString());
   if(res.errorMessage == null){

     Fluttertoast.showToast(
         msg: 'Login Success',
         toastLength: Toast.LENGTH_SHORT,
         gravity: ToastGravity.BOTTOM,
         timeInSecForIosWeb: 3,
         backgroundColor: Colors.green,
         textColor: Colors.white,
         fontSize: 16.0);

     Navigator.push(
       context,
       MaterialPageRoute(builder: (context) => EducationListScreen()),
     );
     if (mounted) {
       setState(() {
         isLoading = false;
       });
     }
   } else {
     if (mounted) {
       setState(() {
         isLoading = false;
       });
     }
     Fluttertoast.showToast(
         msg: res.errorMessage.toString(),
         toastLength: Toast.LENGTH_SHORT,
         gravity: ToastGravity.BOTTOM,
         timeInSecForIosWeb: 3,
         backgroundColor: Colors.red,
         textColor: Colors.white,
         fontSize: 16.0);


   }
    }
  }

}
