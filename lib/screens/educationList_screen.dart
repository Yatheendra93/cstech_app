// ignore_for_file: prefer_const_constructors

import 'dart:convert';

import 'package:devaseva_app/Services/api_client.dart';
import 'package:devaseva_app/models/education_Model.dart';
import 'package:devaseva_app/screens/educationDetails_screen.dart';
import 'package:devaseva_app/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EducationListScreen extends StatefulWidget {
  const EducationListScreen({Key? key}) : super(key: key);

  @override
  _EducationListScreenState createState() => _EducationListScreenState();
}

class _EducationListScreenState extends State<EducationListScreen> {
  bool searchState = false;

  ApiClient client = ApiClient();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.menu,
              color: Colors.white,
              size: 25,
            )),
        title: !searchState
            ? Text(
                "Education List",
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontFamily: "Poppins",
                    fontSize: 20),
              )
            : TextField(
                decoration: InputDecoration(
                  icon: Icon(Icons.search),
                  hintText: "Search...",
                  hintStyle: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: "Poppins"),
                ),
                onChanged: (text) {
                  // SearchMethod(text);
                },
              ),
        actions: [
          !searchState
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      searchState = !searchState;
                    });
                  },
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 25,
                  ))
              : IconButton(
                  onPressed: () {
                    setState(() {
                      searchState = !searchState;
                    });
                  },
                  icon: Icon(
                    Icons.cancel,
                    color: Colors.white,
                    size: 25,
                  )),
          !searchState
              ? IconButton(
                  onPressed: () {
                    showAlertDialog(context);
                  },
                  icon: Icon(
                    Icons.power_settings_new,
                    color: Colors.white,
                    size: 25,
                  ))
              : Container(),
          SizedBox(
            width: 8,
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 12.0),
          child: Column(
            // shrinkWrap: true,
            // scrollDirection: Axis.vertical,
            children: [
              FutureBuilder<EducationResponse>(
                future: client.fetchData(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data!.model!.length,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.only(bottom: 8.0),
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(8.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: InkWell(
                            onTap: (){
                              Navigator.of(context).push(
                                 MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      EducationDetailsScreen(educationId: snapshot.data!.model![index].educationId,),
                                ),
                              );
                            },
                            child: ListTile(
                                title: Text(snapshot.data!.model![index].degree
                                    .toString(),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Poppins",
                                      fontSize: 18),
                                )),
                          ),
                        );
                      },
                    );
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }
                  return Center(child: const CircularProgressIndicator());
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showAlertDialog(BuildContext context) {
    Widget cancelButton = TextButton(
      child: Text("Cancel",style: TextStyle(fontFamily: "Poppins"),),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        "LogOut",
        style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black,fontFamily: "Poppins" ),
      ),
      onPressed: () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => LoginScreen()),
        );
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Logout",
          style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black,fontFamily: "Poppins")),
      content: Text(
        "Are you sure want to logout from your app ?",
        style: TextStyle(fontFamily: "Poppins"),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

