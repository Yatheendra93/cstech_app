// To parse this JSON data, do
//
//     final educationResponse = educationResponseFromJson(jsonString);

// ignore_for_file: prefer_if_null_operators

import 'dart:convert';

EducationResponse educationResponseFromJson(String str) => EducationResponse.fromJson(json.decode(str));

String educationResponseToJson(EducationResponse data) => json.encode(data.toJson());

class EducationResponse {
  EducationResponse({
    this.message,
    this.didError,
    this.errorMessage,
    this.model,
    this.pageSize,
    this.pageNumber,
    this.itemsCount,
    this.pageCount,
  });

  dynamic message;
  bool? didError;
  dynamic errorMessage;
  List<Model>? model;
  int? pageSize;
  int? pageNumber;
  int? itemsCount;
  double? pageCount;

  factory EducationResponse.fromJson(Map<String, dynamic> json) => EducationResponse(
    message: json["message"],
    didError: json["didError"] == null ? null : json["didError"],
    errorMessage: json["errorMessage"],
    model: json["model"] == null ? null : List<Model>.from(json["model"].map((x) => Model.fromJson(x))),
    pageSize: json["pageSize"] == null ? null : json["pageSize"],
    pageNumber: json["pageNumber"] == null ? null : json["pageNumber"],
    itemsCount: json["itemsCount"] == null ? null : json["itemsCount"],
    pageCount: json["pageCount"] == null ? null : json["pageCount"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "didError": didError == null ? null : didError,
    "errorMessage": errorMessage,
    "model": model == null ? null : List<dynamic>.from(model!.map((x) => x.toJson())),
    "pageSize": pageSize == null ? null : pageSize,
    "pageNumber": pageNumber == null ? null : pageNumber,
    "itemsCount": itemsCount == null ? null : itemsCount,
    "pageCount": pageCount == null ? null : pageCount,
  };
}

class Model {
  Model({
    this.educationId,
    this.degree,
    this.educationType,
    this.jobseekerEducation,
  });

  int? educationId;
  String? degree;
  List<dynamic>? educationType;
  List<dynamic>? jobseekerEducation;

  factory Model.fromJson(Map<String, dynamic> json) => Model(
    educationId: json["educationId"] == null ? null : json["educationId"],
    degree: json["degree"] == null ? null : json["degree"],
    educationType: json["educationType"] == null ? null : List<dynamic>.from(json["educationType"].map((x) => x)),
    jobseekerEducation: json["jobseekerEducation"] == null ? null : List<dynamic>.from(json["jobseekerEducation"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "educationId": educationId == null ? null : educationId,
    "degree": degree == null ? null : degree,
    "educationType": educationType == null ? null : List<dynamic>.from(educationType!.map((x) => x)),
    "jobseekerEducation": jobseekerEducation == null ? null : List<dynamic>.from(jobseekerEducation!.map((x) => x)),
  };
}
