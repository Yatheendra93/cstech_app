// To parse this JSON data, do
//
//     final educationByIdResponse = educationByIdResponseFromJson(jsonString);

// ignore_for_file: prefer_if_null_operators

import 'dart:convert';

EducationByIdResponse educationByIdResponseFromJson(String str) => EducationByIdResponse.fromJson(json.decode(str));

String educationByIdResponseToJson(EducationByIdResponse data) => json.encode(data.toJson());

class EducationByIdResponse {
  EducationByIdResponse({
    this.message,
    this.didError,
    this.errorMessage,
    this.model,
    this.pageSize,
    this.pageNumber,
    this.itemsCount,
    this.pageCount,
  });

  dynamic message;
  bool? didError;
  dynamic errorMessage;
  List<Model>? model;
  int? pageSize;
  int? pageNumber;
  int? itemsCount;
  double? pageCount;

  factory EducationByIdResponse.fromJson(Map<String, dynamic> json) => EducationByIdResponse(
    message: json["message"],
    didError: json["didError"] == null ? null : json["didError"],
    errorMessage: json["errorMessage"],
    model: json["model"] == null ? null : List<Model>.from(json["model"].map((x) => Model.fromJson(x))),
    pageSize: json["pageSize"] == null ? null : json["pageSize"],
    pageNumber: json["pageNumber"] == null ? null : json["pageNumber"],
    itemsCount: json["itemsCount"] == null ? null : json["itemsCount"],
    pageCount: json["pageCount"] == null ? null : json["pageCount"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "didError": didError == null ? null : didError,
    "errorMessage": errorMessage,
    "model": model == null ? null : List<dynamic>.from(model!.map((x) => x.toJson())),
    "pageSize": pageSize == null ? null : pageSize,
    "pageNumber": pageNumber == null ? null : pageNumber,
    "itemsCount": itemsCount == null ? null : itemsCount,
    "pageCount": pageCount == null ? null : pageCount,
  };
}

class Model {
  Model({
    this.educationTypeId,
    this.educationTypeName,
    this.educationId,
    this.education,
    this.jobseekerEducation,
  });

  int? educationTypeId;
  String? educationTypeName;
  int? educationId;
  dynamic education;
  List<dynamic>? jobseekerEducation;

  factory Model.fromJson(Map<String, dynamic> json) => Model(
    educationTypeId: json["educationTypeId"] == null ? null : json["educationTypeId"],
    educationTypeName: json["educationTypeName"] == null ? null : json["educationTypeName"],
    educationId: json["educationId"] == null ? null : json["educationId"],
    education: json["education"],
    jobseekerEducation: json["jobseekerEducation"] == null ? null : List<dynamic>.from(json["jobseekerEducation"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "educationTypeId": educationTypeId == null ? null : educationTypeId,
    "educationTypeName": educationTypeName == null ? null : educationTypeName,
    "educationId": educationId == null ? null : educationId,
    "education": education,
    "jobseekerEducation": jobseekerEducation == null ? null : List<dynamic>.from(jobseekerEducation!.map((x) => x)),
  };
}
